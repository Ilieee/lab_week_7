package com.springSecurityUpdated.springSecurityUpdated.controller;

import com.springSecurityUpdated.springSecurityUpdated.model.User;
import com.springSecurityUpdated.springSecurityUpdated.repository.UserRepo;

import com.springSecurityUpdated.springSecurityUpdated.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller

public class AppController {

    @Autowired
    private UserRepo userRepo;


    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/")
    public String goHome() {
        return "home";
    }

    @PostMapping("/user/save")
    public String saveUser(@RequestBody User user, Model model) {
        if (userRepo.findByEmail(user.getEmail()).isPresent()) {
            System.out.println("O e-mail já está sendo usado.");
            model.addAttribute("message", "O e-mail já está sendo usado.");
        } else {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepo.save(user);
            model.addAttribute("message", "Usuário adicionado com sucesso");
            model.addAttribute("user", user);
        }
        return "user_summary";
    }
    @GetMapping("/admin")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String adminPanel(Model model) {
        return "admin";
    }
    @GetMapping("/user")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public String userPage(Model model) {
        return "user";
    }
}