package com.springSecurityUpdated.springSecurityUpdated.service;

import com.springSecurityUpdated.springSecurityUpdated.model.User;
import com.springSecurityUpdated.springSecurityUpdated.repository.UserRepo;
import com.springSecurityUpdated.springSecurityUpdated.service.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

@Configuration
public class UserInfoUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepo.findByEmail(username);
        return user.map(u -> new UserDetailsImpl(u)).orElseThrow(() -> new UsernameNotFoundException("User Does Not Exist"));
    }
}
