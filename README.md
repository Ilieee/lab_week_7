# Spring Security Updated

Este é um projeto simples que demonstra o uso do Spring Security para autenticação e autorização de usuários em uma aplicação web. O projeto inclui um sistema de login, páginas de usuário e admin, e a funcionalidade de adicionar novos usuários.

## Funcionalidades

- **Login**: Os usuários podem fazer login usando suas credenciais.
- **Páginas de Usuário e Admin**: Páginas protegidas que só podem ser acessadas após a autenticação bem-sucedida.
- **Adicionar Usuários**: Funcionalidade para adicionar novos usuários ao sistema.

## Tecnologias Utilizadas

- **Spring Boot**: Framework para o desenvolvimento de aplicativos Java.
- **Spring Security**: Framework de autenticação e autorização para aplicativos Spring.
- **Thymeleaf**: Motor de template para construir a interface do usuário.
- **Hibernate**: Framework ORM para mapeamento objeto-relacional.

## Como Executar o Projeto

1. Clone este repositório para o seu ambiente local.
2. Certifique-se de ter o Java e o Maven instalados na sua máquina.
3. Navegue até o diretório raiz do projeto e execute `mvn spring-boot:run` no terminal.
4. Abra um navegador da web e acesse `http://localhost:8080` para visualizar a aplicação.